﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace TF2RandomLoadout
{
    public partial class Form1 : Form
    {

        private const string _SOURCE = "data.json";
        private Random _rng;
        private Team team;
        

        public Form1()
        {
            InitializeComponent();
            var json = File.ReadAllText(_SOURCE);
            team = JsonConvert.DeserializeObject<Team>(json);
            _rng = new Random(Guid.NewGuid().GetHashCode());

            primaryLabel.Text =
                secondaryLabel.Text =
                meleeLabel.Text =
                sapperLabel.Text = String.Empty;

            Text = "TF2 Random Loadout";
        }

        private void randomBtn_Click(object sender, EventArgs e)
        {
            switch(_rng.Next(9))
            {
                case 0: scoutBtn_Click(sender, e); break;
                case 1: soldierBtn_Click(sender, e); break;
                case 2: pyroBtn_Click(sender, e); break;
                case 3: demoBtn_Click(sender, e); break;
                case 4: heavyBtn_Click(sender, e); break;
                case 5: engyBtn_Click(sender, e); break;
                case 6: medicBtn_Click(sender, e); break;
                case 7: sniperBtn_Click(sender, e); break;
                case 8: spyBtn_Click(sender, e); break;
                default: break;
            }
        }

        private void randomOff_Click(object sender, EventArgs e)
        {
            switch (_rng.Next(3))
            {
                case 0: scoutBtn_Click(sender, e); break;
                case 1: soldierBtn_Click(sender, e); break;
                case 2: pyroBtn_Click(sender, e); break;
                default: break;
            }
        }

        private void randomDef_Click(object sender, EventArgs e)
        {
            switch (_rng.Next(3))
            {
                case 0: demoBtn_Click(sender, e); break;
                case 1: heavyBtn_Click(sender, e); break;
                case 2: engyBtn_Click(sender, e); break;
                default: break;
            }
        }

        private void randomSup_Click(object sender, EventArgs e)
        {
            switch (_rng.Next(3))
            {
                case 0: medicBtn_Click(sender, e); break;
                case 1: sniperBtn_Click(sender, e); break;
                case 2: spyBtn_Click(sender, e); break;
                default: break;
            }
        }

        private void scoutBtn_Click(object sender, EventArgs e) =>
            apply(team.Scout.GetLoadout(_rng), team.Scout);

        private void soldierBtn_Click(object sender, EventArgs e) =>
            apply(team.Soldier.GetLoadout(_rng), team.Soldier);

        private void pyroBtn_Click(object sender, EventArgs e) =>
            apply(team.Pyro.GetLoadout(_rng), team.Pyro);

        private void demoBtn_Click(object sender, EventArgs e) =>
            apply(team.Demo.GetLoadout(_rng), team.Demo);

        private void heavyBtn_Click(object sender, EventArgs e) =>
            apply(team.Heavy.GetLoadout(_rng), team.Heavy);

        private void engyBtn_Click(object sender, EventArgs e) =>
            apply(team.Engy.GetLoadout(_rng), team.Engy);

        private void medicBtn_Click(object sender, EventArgs e) =>
            apply(team.Medic.GetLoadout(_rng), team.Medic);

        private void sniperBtn_Click(object sender, EventArgs e) =>
            apply(team.Sniper.GetLoadout(_rng), team.Sniper);

        private void spyBtn_Click(object sender, EventArgs e) =>
            apply(team.Spy.GetLoadout(_rng), team.Spy, true);

        private void apply(List<Weapon> loadout, Player player, bool stretch = false)
        {
            classPicture.Size = new Size {
                Height = stretch ? 317 : 131,
                Width = classPicture.Size.Width
            };

            classPicture.ImageLocation = player.Path;
            primaryPicture.ImageLocation = loadout[0].Path;
            secondaryPicture.ImageLocation = loadout[1].Path;
            meleePicture.ImageLocation = loadout[2].Path;
            if (loadout.Count > 3)
                sapperPicture.ImageLocation = loadout[3].Path;
            else
                sapperPicture.Image = null;

            label.Text = player.Name;
            sapperLabel.Text = loadout.Count > 3 ? loadout[3].Name : String.Empty;
            primaryLabel.Text = loadout[0].Name;
            secondaryLabel.Text = loadout[1].Name;
            meleeLabel.Text = loadout[2].Name;
        }

        internal class Team
        {
            public Player Scout { get; set; }
            public Player Soldier { get; set; }
            public Player Pyro { get; set; }
            public Player Demo { get; set; }
            public Player Heavy { get; set; }
            public Player Engy { get; set; }
            public Player Medic { get; set; }
            public Player Sniper { get; set; }
            public SpyPlayer Spy { get; set; }
        }

        internal class Player
        {
            public string Path { get; set; }
            public string Name { get; set; }
            public List<Weapon> Primaries { get; set; }
            public List<Weapon> Secondaries { get; set; }
            public List<Weapon> Melees { get; set; }

            public virtual List<Weapon> GetLoadout(Random rng)
            {
                return new List<Weapon> {
                    Primaries[rng.Next(Primaries.Count)],
                    Secondaries[rng.Next(Secondaries.Count)],
                    Melees[rng.Next(Melees.Count)]
                };
            }
        }

        internal class SpyPlayer : Player
        {
            public List<Weapon> Sappers { get; set; }

            public override List<Weapon> GetLoadout(Random rng)
            {
                var list = base.GetLoadout(rng);
                list.Add(Sappers[rng.Next(Sappers.Count)]);
                return list;
            }
        }

        internal class Weapon
        {
            public string Path { get; set; }
            public string Name { get; set; }
        }

        private void credLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://steamcommunity.com/id/Vaan");
        }
    }
}
