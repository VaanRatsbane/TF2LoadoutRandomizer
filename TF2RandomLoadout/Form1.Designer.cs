﻿namespace TF2RandomLoadout
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.randomBtn = new System.Windows.Forms.Button();
            this.scoutBtn = new System.Windows.Forms.Button();
            this.pyroBtn = new System.Windows.Forms.Button();
            this.soldierBtn = new System.Windows.Forms.Button();
            this.medicBtn = new System.Windows.Forms.Button();
            this.engyBtn = new System.Windows.Forms.Button();
            this.heavyBtn = new System.Windows.Forms.Button();
            this.demoBtn = new System.Windows.Forms.Button();
            this.spyBtn = new System.Windows.Forms.Button();
            this.sniperBtn = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.classPicture = new System.Windows.Forms.PictureBox();
            this.primaryPicture = new System.Windows.Forms.PictureBox();
            this.secondaryPicture = new System.Windows.Forms.PictureBox();
            this.meleePicture = new System.Windows.Forms.PictureBox();
            this.sapperPicture = new System.Windows.Forms.PictureBox();
            this.primaryLabel = new System.Windows.Forms.Label();
            this.secondaryLabel = new System.Windows.Forms.Label();
            this.meleeLabel = new System.Windows.Forms.Label();
            this.sapperLabel = new System.Windows.Forms.Label();
            this.randomOff = new System.Windows.Forms.Button();
            this.randomDef = new System.Windows.Forms.Button();
            this.randomSup = new System.Windows.Forms.Button();
            this.creds = new System.Windows.Forms.Label();
            this.credLink = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.classPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.primaryPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meleePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sapperPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // randomBtn
            // 
            this.randomBtn.Location = new System.Drawing.Point(125, 160);
            this.randomBtn.Name = "randomBtn";
            this.randomBtn.Size = new System.Drawing.Size(106, 23);
            this.randomBtn.TabIndex = 1;
            this.randomBtn.Text = "Random";
            this.randomBtn.UseVisualStyleBackColor = true;
            this.randomBtn.Click += new System.EventHandler(this.randomBtn_Click);
            // 
            // scoutBtn
            // 
            this.scoutBtn.Location = new System.Drawing.Point(12, 15);
            this.scoutBtn.Name = "scoutBtn";
            this.scoutBtn.Size = new System.Drawing.Size(106, 23);
            this.scoutBtn.TabIndex = 2;
            this.scoutBtn.Text = "Scout";
            this.scoutBtn.UseVisualStyleBackColor = true;
            this.scoutBtn.Click += new System.EventHandler(this.scoutBtn_Click);
            // 
            // pyroBtn
            // 
            this.pyroBtn.Location = new System.Drawing.Point(12, 73);
            this.pyroBtn.Name = "pyroBtn";
            this.pyroBtn.Size = new System.Drawing.Size(106, 23);
            this.pyroBtn.TabIndex = 4;
            this.pyroBtn.Text = "Pyro";
            this.pyroBtn.UseVisualStyleBackColor = true;
            this.pyroBtn.Click += new System.EventHandler(this.pyroBtn_Click);
            // 
            // soldierBtn
            // 
            this.soldierBtn.Location = new System.Drawing.Point(12, 44);
            this.soldierBtn.Name = "soldierBtn";
            this.soldierBtn.Size = new System.Drawing.Size(106, 23);
            this.soldierBtn.TabIndex = 3;
            this.soldierBtn.Text = "Soldier";
            this.soldierBtn.UseVisualStyleBackColor = true;
            this.soldierBtn.Click += new System.EventHandler(this.soldierBtn_Click);
            // 
            // medicBtn
            // 
            this.medicBtn.Location = new System.Drawing.Point(12, 189);
            this.medicBtn.Name = "medicBtn";
            this.medicBtn.Size = new System.Drawing.Size(106, 23);
            this.medicBtn.TabIndex = 8;
            this.medicBtn.Text = "Medic";
            this.medicBtn.UseVisualStyleBackColor = true;
            this.medicBtn.Click += new System.EventHandler(this.medicBtn_Click);
            // 
            // engyBtn
            // 
            this.engyBtn.Location = new System.Drawing.Point(12, 160);
            this.engyBtn.Name = "engyBtn";
            this.engyBtn.Size = new System.Drawing.Size(106, 23);
            this.engyBtn.TabIndex = 7;
            this.engyBtn.Text = "Engy";
            this.engyBtn.UseVisualStyleBackColor = true;
            this.engyBtn.Click += new System.EventHandler(this.engyBtn_Click);
            // 
            // heavyBtn
            // 
            this.heavyBtn.Location = new System.Drawing.Point(12, 131);
            this.heavyBtn.Name = "heavyBtn";
            this.heavyBtn.Size = new System.Drawing.Size(106, 23);
            this.heavyBtn.TabIndex = 6;
            this.heavyBtn.Text = "Heavy";
            this.heavyBtn.UseVisualStyleBackColor = true;
            this.heavyBtn.Click += new System.EventHandler(this.heavyBtn_Click);
            // 
            // demoBtn
            // 
            this.demoBtn.Location = new System.Drawing.Point(12, 102);
            this.demoBtn.Name = "demoBtn";
            this.demoBtn.Size = new System.Drawing.Size(106, 23);
            this.demoBtn.TabIndex = 5;
            this.demoBtn.Text = "Demo";
            this.demoBtn.UseVisualStyleBackColor = true;
            this.demoBtn.Click += new System.EventHandler(this.demoBtn_Click);
            // 
            // spyBtn
            // 
            this.spyBtn.Location = new System.Drawing.Point(12, 247);
            this.spyBtn.Name = "spyBtn";
            this.spyBtn.Size = new System.Drawing.Size(106, 23);
            this.spyBtn.TabIndex = 10;
            this.spyBtn.Text = "Spy";
            this.spyBtn.UseVisualStyleBackColor = true;
            this.spyBtn.Click += new System.EventHandler(this.spyBtn_Click);
            // 
            // sniperBtn
            // 
            this.sniperBtn.Location = new System.Drawing.Point(12, 218);
            this.sniperBtn.Name = "sniperBtn";
            this.sniperBtn.Size = new System.Drawing.Size(106, 23);
            this.sniperBtn.TabIndex = 9;
            this.sniperBtn.Text = "Sniper";
            this.sniperBtn.UseVisualStyleBackColor = true;
            this.sniperBtn.Click += new System.EventHandler(this.sniperBtn_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(255, 15);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 24);
            this.label.TabIndex = 11;
            // 
            // classPicture
            // 
            this.classPicture.Location = new System.Drawing.Point(124, 15);
            this.classPicture.Name = "classPicture";
            this.classPicture.Size = new System.Drawing.Size(125, 131);
            this.classPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.classPicture.TabIndex = 12;
            this.classPicture.TabStop = false;
            // 
            // primaryPicture
            // 
            this.primaryPicture.Location = new System.Drawing.Point(256, 62);
            this.primaryPicture.Name = "primaryPicture";
            this.primaryPicture.Size = new System.Drawing.Size(100, 75);
            this.primaryPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.primaryPicture.TabIndex = 13;
            this.primaryPicture.TabStop = false;
            // 
            // secondaryPicture
            // 
            this.secondaryPicture.Location = new System.Drawing.Point(378, 62);
            this.secondaryPicture.Name = "secondaryPicture";
            this.secondaryPicture.Size = new System.Drawing.Size(100, 75);
            this.secondaryPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.secondaryPicture.TabIndex = 14;
            this.secondaryPicture.TabStop = false;
            // 
            // meleePicture
            // 
            this.meleePicture.Location = new System.Drawing.Point(259, 184);
            this.meleePicture.Name = "meleePicture";
            this.meleePicture.Size = new System.Drawing.Size(100, 75);
            this.meleePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.meleePicture.TabIndex = 15;
            this.meleePicture.TabStop = false;
            // 
            // sapperPicture
            // 
            this.sapperPicture.Location = new System.Drawing.Point(378, 184);
            this.sapperPicture.Name = "sapperPicture";
            this.sapperPicture.Size = new System.Drawing.Size(100, 75);
            this.sapperPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.sapperPicture.TabIndex = 16;
            this.sapperPicture.TabStop = false;
            // 
            // primaryLabel
            // 
            this.primaryLabel.AutoSize = true;
            this.primaryLabel.Location = new System.Drawing.Point(256, 43);
            this.primaryLabel.Name = "primaryLabel";
            this.primaryLabel.Size = new System.Drawing.Size(35, 13);
            this.primaryLabel.TabIndex = 17;
            this.primaryLabel.Text = "label1";
            // 
            // secondaryLabel
            // 
            this.secondaryLabel.AutoSize = true;
            this.secondaryLabel.Location = new System.Drawing.Point(375, 43);
            this.secondaryLabel.Name = "secondaryLabel";
            this.secondaryLabel.Size = new System.Drawing.Size(35, 13);
            this.secondaryLabel.TabIndex = 18;
            this.secondaryLabel.Text = "label1";
            // 
            // meleeLabel
            // 
            this.meleeLabel.AutoSize = true;
            this.meleeLabel.Location = new System.Drawing.Point(256, 159);
            this.meleeLabel.Name = "meleeLabel";
            this.meleeLabel.Size = new System.Drawing.Size(35, 13);
            this.meleeLabel.TabIndex = 19;
            this.meleeLabel.Text = "label1";
            // 
            // sapperLabel
            // 
            this.sapperLabel.AutoSize = true;
            this.sapperLabel.Location = new System.Drawing.Point(375, 159);
            this.sapperLabel.Name = "sapperLabel";
            this.sapperLabel.Size = new System.Drawing.Size(35, 13);
            this.sapperLabel.TabIndex = 20;
            this.sapperLabel.Text = "label1";
            // 
            // randomOff
            // 
            this.randomOff.Location = new System.Drawing.Point(125, 189);
            this.randomOff.Name = "randomOff";
            this.randomOff.Size = new System.Drawing.Size(106, 23);
            this.randomOff.TabIndex = 21;
            this.randomOff.Text = "Random Offense";
            this.randomOff.UseVisualStyleBackColor = true;
            this.randomOff.Click += new System.EventHandler(this.randomOff_Click);
            // 
            // randomDef
            // 
            this.randomDef.Location = new System.Drawing.Point(125, 218);
            this.randomDef.Name = "randomDef";
            this.randomDef.Size = new System.Drawing.Size(106, 23);
            this.randomDef.TabIndex = 22;
            this.randomDef.Text = "Random Defense";
            this.randomDef.UseVisualStyleBackColor = true;
            this.randomDef.Click += new System.EventHandler(this.randomDef_Click);
            // 
            // randomSup
            // 
            this.randomSup.Location = new System.Drawing.Point(125, 247);
            this.randomSup.Name = "randomSup";
            this.randomSup.Size = new System.Drawing.Size(106, 23);
            this.randomSup.TabIndex = 23;
            this.randomSup.Text = "Random Support";
            this.randomSup.UseVisualStyleBackColor = true;
            this.randomSup.Click += new System.EventHandler(this.randomSup_Click);
            // 
            // creds
            // 
            this.creds.AutoSize = true;
            this.creds.Location = new System.Drawing.Point(166, 273);
            this.creds.Name = "creds";
            this.creds.Size = new System.Drawing.Size(272, 13);
            this.creds.TabIndex = 24;
            this.creds.Text = "Images pulled from wiki.teamfortress.com. Tool made by ";
            // 
            // credLink
            // 
            this.credLink.ActiveLinkColor = System.Drawing.Color.RoyalBlue;
            this.credLink.AutoSize = true;
            this.credLink.LinkColor = System.Drawing.Color.RoyalBlue;
            this.credLink.Location = new System.Drawing.Point(433, 273);
            this.credLink.Name = "credLink";
            this.credLink.Size = new System.Drawing.Size(32, 13);
            this.credLink.TabIndex = 25;
            this.credLink.TabStop = true;
            this.credLink.Text = "Vaan";
            this.credLink.VisitedLinkColor = System.Drawing.Color.RoyalBlue;
            this.credLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.credLink_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 293);
            this.Controls.Add(this.credLink);
            this.Controls.Add(this.creds);
            this.Controls.Add(this.randomSup);
            this.Controls.Add(this.randomDef);
            this.Controls.Add(this.randomOff);
            this.Controls.Add(this.sapperLabel);
            this.Controls.Add(this.meleeLabel);
            this.Controls.Add(this.secondaryLabel);
            this.Controls.Add(this.primaryLabel);
            this.Controls.Add(this.sapperPicture);
            this.Controls.Add(this.meleePicture);
            this.Controls.Add(this.secondaryPicture);
            this.Controls.Add(this.primaryPicture);
            this.Controls.Add(this.label);
            this.Controls.Add(this.spyBtn);
            this.Controls.Add(this.sniperBtn);
            this.Controls.Add(this.medicBtn);
            this.Controls.Add(this.engyBtn);
            this.Controls.Add(this.heavyBtn);
            this.Controls.Add(this.demoBtn);
            this.Controls.Add(this.pyroBtn);
            this.Controls.Add(this.soldierBtn);
            this.Controls.Add(this.scoutBtn);
            this.Controls.Add(this.randomBtn);
            this.Controls.Add(this.classPicture);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "<";
            ((System.ComponentModel.ISupportInitialize)(this.classPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.primaryPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meleePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sapperPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button randomBtn;
        private System.Windows.Forms.Button scoutBtn;
        private System.Windows.Forms.Button pyroBtn;
        private System.Windows.Forms.Button soldierBtn;
        private System.Windows.Forms.Button medicBtn;
        private System.Windows.Forms.Button engyBtn;
        private System.Windows.Forms.Button heavyBtn;
        private System.Windows.Forms.Button demoBtn;
        private System.Windows.Forms.Button spyBtn;
        private System.Windows.Forms.Button sniperBtn;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.PictureBox classPicture;
        private System.Windows.Forms.PictureBox primaryPicture;
        private System.Windows.Forms.PictureBox secondaryPicture;
        private System.Windows.Forms.PictureBox meleePicture;
        private System.Windows.Forms.PictureBox sapperPicture;
        private System.Windows.Forms.Label primaryLabel;
        private System.Windows.Forms.Label secondaryLabel;
        private System.Windows.Forms.Label meleeLabel;
        private System.Windows.Forms.Label sapperLabel;
        private System.Windows.Forms.Button randomOff;
        private System.Windows.Forms.Button randomDef;
        private System.Windows.Forms.Button randomSup;
        private System.Windows.Forms.Label creds;
        private System.Windows.Forms.LinkLabel credLink;
    }
}

